import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

public class XMLtoPDF
{
    //Declare variables for XSL location and PDF location
    public static String SOURCE;
    public static String OUTPUT;

    //Method converts given XML and XSL files to PDF
    public static void convert() throws IOException, FOPException, TransformerException {

        //Defines the source and output directories
        SOURCE = "resources//";
        OUTPUT = "resources//output//";

        //Declare requirements
        File xslFile;
        StreamSource xmlFile;
        FopFactory fopFactory;
        FOUserAgent userAgent;
        OutputStream outStream;

        //Grabs XSL File
        xslFile = new File(SOURCE + "//timetableLayout.xsl");
        //Grabs XML file
        xmlFile = new StreamSource(new File(SOURCE + "//timetableData.xml"));
        //Creates an FOP factory
        fopFactory = FopFactory.newInstance(new File(".").toURI());
        //Creates a user agent from FOP factory
        userAgent = fopFactory.newFOUserAgent();
        //Sets up output and shows where to place PDF file
        outStream = new java.io.FileOutputStream(OUTPUT + "//timetableRulesPDF.pdf");

        try {
            //Declare requirements
            Fop fop;
            TransformerFactory factory;
            Transformer transformer;
            Result result;

            //Construct FOP for PDF output
            fop = fopFactory.newFop(MimeConstants.MIME_PDF, userAgent, outStream);

            //Sets up XSL transformation
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource(xslFile));

            //SAX (Simple API for XML) event
            //Pipes generated FO to FOP
            result = new SAXResult(fop.getDefaultHandler());

            // Does XSL transformation and FOP processing to create PDF
            transformer.transform(xmlFile, result);
        } finally {
            //Closes file stream
            outStream.close();
        }
    }

    public static void main( String[] args )
    {
        //If method fails, catch exceptions will show what's wrong
        //and print error message, otherwise PDF if saved
        try {
            convert();
        } catch (FOPException | IOException | TransformerException e) {
            e.printStackTrace();
            System.out.print("ERROR - CONVERSION FAILED");
        }
    }
}
