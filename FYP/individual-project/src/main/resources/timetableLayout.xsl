<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="xml" standalone="no" omit-xml-declaration="no"/>
	<xsl:template match="timetable">
		<fo:root language="IT" xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="cover" page-height="297mm" page-width="210mm" margin-top="5mm" margin-bottom="5mm" margin-left="5mm" margin-right="5mm">
		            <fo:region-body margin-top="25mm" margin-bottom="20mm"/>
        		</fo:simple-page-master> 
				<fo:simple-page-master master-name="A4-portrail" page-height="297mm" page-width="210mm" margin-top="5mm" margin-bottom="5mm" margin-left="5mm" margin-right="5mm">
					<fo:region-body margin-top="25mm" margin-bottom="20mm"/>
					<fo:region-before region-name="xsl-region-before" extent="25mm" display-align="before" precedence="true"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="cover">
				<fo:flow flow-name="xsl-region-body">
					<fo:block font-size="24pt" text-align="center"> TIMETABLE PLANNING RULES</fo:block>
					<fo:block space-before="3mm"/>
					<fo:block font-size="18pt" text-align="center">London North Western</fo:block>
					<fo:block space-before="3mm"/>
					<fo:block font-size="12pt" text-align="center">2018 Timetable</fo:block>
					<fo:block space-before="3mm"/>
					<fo:block font-weight="bold" font-size="12pt" text-align="center">Version 4.1</fo:block>
					<fo:block space-before="120mm"/>
					<fo:block font-size="12pt" text-align="left">Issued by</fo:block>
					<fo:block space-before="3mm"/>
					<fo:block font-size="12pt" text-align="left">Hannah Linford</fo:block>
					<fo:block font-size="12pt" text-align="left">Timetable Production Manager for London North Western</fo:block>
					<fo:block font-size="12pt" text-align="left">The Quadrant</fo:block>
					<fo:block font-size="12pt" text-align="left">Elder Gate</fo:block>
					<fo:block font-size="12pt" text-align="left">Milton Keynes Central</fo:block>
					<fo:block font-size="12pt" text-align="left">Buckinghamshire</fo:block>
					<fo:block font-size="12pt" text-align="left">MK9 1EN</fo:block>
					<fo:block space-before="20mm"/>
					<fo:block font-size="12pt" text-align="left">Final Principal and Final Proposal for Subsidiary Change Timetable 2018</fo:block>
					<fo:block font-size="12pt" text-align="left">15th September 2017</fo:block>
				</fo:flow>
			</fo:page-sequence>
			<fo:page-sequence master-reference="A4-portrail">
<!-- Start of Header Table -->
				<fo:static-content flow-name="xsl-region-before">
					<fo:table table-layout="fixed" width="100%" font-size="10pt" border-color="black" border-width="0.4mm" border-style="solid">
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(45)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell text-align="left" display-align="center" padding-left="2mm">
									<fo:block>
										NETWORK RAIL
									</fo:block>
									<fo:block>
										Route: LNW
									</fo:block>
								</fo:table-cell>
								<fo:table-cell text-align="center" display-align="center">
									<fo:block>
										Timetable Planning Rules
									</fo:block>
									<fo:block>
										Final Principal and Final Proposal for Subsidary Change
									</fo:block>
									<fo:block>
										Timetable 2018
									</fo:block>
									<fo:block space-before="3mm"/>
								</fo:table-cell>
								<fo:table-cell text-align="right" display-align="center" padding-right="2mm">
									<fo:block>
										Version: 4.1
									</fo:block>
									<fo:block>
										Date: 15th Sept. 2017
									</fo:block>
									<fo:block display-align="before" space-before="6mm">Page <fo:page-number/> of <fo:page-number-citation ref-id="end-of-document"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
<!-- End of Header Table -->
				<fo:flow flow-name="xsl-region-body" border-collapse="collapse" reference-orientation="0">
<!-- Start of Headways Intro -->
					<fo:block font-size="120%">5.2 Headway Values</fo:block>
					<fo:block space-before="3mm"/>
					<fo:block>All times are in minutes. All routes are shown.</fo:block> 
					<fo:block space-before="3mm"/>
					<fo:block>Where track circuit block (TCB) signalling applies, the standard headways for each route are shown, together with any exceptions.</fo:block>
					<fo:block space-before="3mm"/>
					<fo:block>AB indicates locations where absolute block signalling applies: here the headway is to be calculated from the transit time of the first of each pair of trains running between the consecutive block posts being considered. To this transit time shall be added 2 minutes to allow for the signaller’s actions. Exceptions are shown as AB and appear together with the actual headway value to be used, which includes the allowance for signallers’ actions. Where there is an intermediate block signal, the absolute block section concerned shall be between this signal and the next block post in advance.</fo:block>
					<fo:block space-before="3mm"/>
					<fo:block>Single lines and other forms of signalling are shown, together with any values applicable, where they occur.</fo:block>
					<fo:block space-before="3mm"/>
					<fo:block>"OTNS" or "OT" indicates One Train Working with No Train Staff; "OTS" or "OT(S)" indicates One train Working with Train Staff. "NST" indicates No Signaller token. In these cases only one train is allowed in the section at one time; a second train cannot be allowed to enter the section until the first train has left the section.</fo:block>
					<fo:block space-before="3mm"/>
					<fo:block>"ETB" indicates Electric Token Block and "TB" indicate Tokenless Block for single lines.</fo:block>
					<fo:block space-before="3mm"/>
					<fo:block>"RB" indicates Radio Signalling where "long section tokens" can be issued between certain block posts during times of low traffic volume.</fo:block>
					<fo:block space-before="3mm"/>

<!-- Start of Headways Table -->
					<xsl:for-each select="headways/journey">
					<fo:table table-layout="fixed" width="100%" font-size="10pt" border-color="black" border-width="0.35mm" border-style="solid" text-align="left" display-align="center" space-after="5mm">
						<fo:table-column column-width="proportional-column-width(35)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(40)"/>
						<fo:table-body font-size="100%">
							<fo:table-row height="3mm" font-weight="bold" border-color="black" border-width="0.35mm" border-style="solid" margin-left="1mm">
								<fo:table-cell number-columns-spanned="4" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block><xsl:value-of select="journeyName"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
									<xsl:choose>
										<xsl:when test="timingPointHeader=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="4" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
									<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="timingPointHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="notesHeader"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="timingPointInfo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downData"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upData"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="notesInfo"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:choose>
										<xsl:when test="timingPointInfo2=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="timingPointInfo2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downData2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upData2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="notesInfo2"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="timingPointInfo3=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="timingPointInfo3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downData3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upData3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="notesInfo3"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="timingPointInfo4=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="timingPointInfo4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downData4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upData4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="notesInfo4"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="timingPointInfo5=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="timingPointInfo5"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downData5"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upData5"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="notesInfo5"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="timingPointInfo6=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="timingPointInfo6"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downData6"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upData6"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="notesInfo6"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="timingPointInfo7=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="timingPointInfo7"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downData7"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upData7"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="notesInfo7"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="timingPointInfo8=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="timingPointInfo8"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downData8"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upData8"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="notesInfo8"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="4" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="exceptionsHeader=''">
										</xsl:when>
									<xsl:otherwise>
								<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell number-columns-spanned="4" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exceptionsHeader"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:choose>
									<xsl:when test="exceptionsLocation1=''">
									</xsl:when>
									<xsl:otherwise>
										<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell number-columns-spanned="4" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exceptionsLocation1"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exceptionsFirstMovementHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned ="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exceptionsSecondMovementHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exceptionsValueHeader"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:choose>
									<xsl:when test="exLoc1FMInfo=''">
									</xsl:when>
									<xsl:otherwise>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc1FMInfo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc1SMInfo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc1ValueData"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
									<xsl:when test="exLoc1FMInfo2=''">
									</xsl:when>
									<xsl:otherwise>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc1FMInfo2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc1SMInfo2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc1ValueData2"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
									<xsl:when test="exLoc1FMInfo3=''">
									</xsl:when>
									<xsl:otherwise>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc1FMInfo3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc1SMInfo3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc1ValueData3"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
									<xsl:when test="exLoc1FMInfo4=''">
									</xsl:when>
									<xsl:otherwise>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc1FMInfo4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc1SMInfo4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc1ValueData4"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
									<xsl:when test="exceptionsLocation2=''">
									</xsl:when>
									<xsl:otherwise>
										<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="4" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
										<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell number-columns-spanned="4" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exceptionsLocation2"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exceptionsFirstMovementHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned ="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exceptionsSecondMovementHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exceptionsValueHeader"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc2FMInfo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc2SMInfo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc2ValueData"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:choose>
									<xsl:when test="exLoc2FMInfo2=''">
									</xsl:when>
									<xsl:otherwise>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc2FMInfo2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc2SMInfo2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc2ValueData2"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
									<xsl:when test="exLoc2FMInfo3=''">
									</xsl:when>
									<xsl:otherwise>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc2FMInfo3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc2SMInfo3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc2ValueData3"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
									<xsl:when test="exLoc2FMInfo4=''">
									</xsl:when>
									<xsl:otherwise>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc2FMInfo4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc2SMInfo4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="exLoc2ValueData4"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
						<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="4" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						</fo:table-body>
					</fo:table>
					</xsl:for-each>
					<fo:block space-before="100mm"/>
<!-- End of Headways Table -->
<!-- Start of Junction Margins Intro -->
					<fo:block font-size="120%">5.3 Junction Margins</fo:block>
					<fo:block space-before="3mm"/>
					<fo:block>The definition for Junction Margins and Station Planning Rules is listed in Section 6.6, 6.7 and 6.8 of the National TPRs.</fo:block> 
					<fo:block space-before="3mm"/>
					<fo:block>All times shown are in minutes. Where adjustments to sectional running times are shown, the value must be added to the normal SRTs shown in B Plan. Negative adjustments are specially identified. </fo:block>
					<fo:block space-before="3mm"/>
					<fo:block>Minimum station allowances are the minimum practical for the particular type of stock. These are shown with exceptions being listed by line of route where applicable.</fo:block>
					<fo:block space-before="3mm"/>
<!-- Start of Standard Values Table -->
					<fo:table table-layout="fixed" width="100%" font-size="10pt" border-color="black" border-width="0.35mm" border-style="solid" text-align="left" display-align="center" space-after="5mm">
						<fo:table-column column-width="proportional-column-width(50)"/>
						<fo:table-column column-width="proportional-column-width(50)"/>
						<fo:table-column column-width="proportional-column-width(15)"/>
						<fo:table-body font-size="100%">
							<fo:table-row height="3mm" font-weight="bold" border-color="black" border-width="0.35mm" border-style="solid" margin-left="1mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block>STANDARD VALUES - MINIMUM</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
									<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												Adjustments to Sectional Running Times
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Movements
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Reason
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Value
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Terminating trains arriving on half minutes in final timing link (except trains terminating at Liverpool Central)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Station working
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											1/2
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell font-weight="bold" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Connectional Allowance
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											5
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												Attachment of Locomotives/Units
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell font-weight="bold" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class/Type
										</fo:block>
									</fo:table-cell>
									<fo:table-cell font-weight="bold" number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Allowance
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 22X
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											7
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											DMU
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											6
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											EMU (except Classes 350, 507 and 508)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											3
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 350 EMU (London Midland)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											5
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 350 EMU (First TransPennine Express)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											7
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Classes 507/508 (Merseyrail)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											5
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Locomotive
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											10, 15 if Class 57/3 attaching to Class 390
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												Detachment of Locomotives/Units:
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell font-weight="bold" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class/Type
										</fo:block>
									</fo:table-cell>
									<fo:table-cell font-weight="bold" number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Allowance
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 221 (Virgin Trains)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											7
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 22x (CrossCountry)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											6
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 222 (East Midlands Trains)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											5
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											DMU
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											5
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											EMU (except Classes 350, 507 and 508)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											2
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 350 EMU (London Midland)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											5
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 350 EMU (First TransPennine Express)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											7
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Classes 507/508 (Merseyrail)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											5
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Locomotive
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											10 including detaching Class 57/3 from Class 390
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												Dwell Time
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell font-weight="bold" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Dwell
										</fo:block>
									</fo:table-cell>
									<fo:table-cell font-weight="bold" number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Allowance
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Standard
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											1/2
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 170 and Class 185 DMUs
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											45 seconds. To be shown as alternative 1/2 and 1 minute dwells
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 22x DMU
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											1 1/2
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 350 EMU
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											45 seconds. To be shown as alternative 1/2 and 1 minute dwells
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 390
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											2
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											LH/HST
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											1
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell font-weight="bold" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Junction Margin
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											3 before and 2 after
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												If the first move is a crossing move, a margin of 3 minutes shall apply before the second move. If the second move is a crossing move, a margin of 2 minutes shall apply to the second move. 
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell font-weight="bold" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Minimum Loco Change Allowance
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											12
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell font-weight="bold" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Minimum Loco Run-round Allowance
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											15 Passenger, 20 Freight
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell font-weight="bold" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Minimum Turnaround
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											10, except for the following
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Terminating CrossCountry trains prior to ECS move to Depot - Safety Check Unit (SCU):-
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											1 x Class 22X *
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											8 SCU and depart in same direction as arrival
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											1 x Class 22X
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											10 SCU and depart in opposite direction to arrival
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											2 x Class 22X *
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											15 SCU and depart in same direction as arrival
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											2 x Class 22X 
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											20 SCU and depart in opposite direction to arrival
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											LH/HST
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											15
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											DMU/EMU
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											4 But no more than 3 successive 4 minutes turnarounds followed by an additional 10 minutes. (10 minutes applies to diagrams and not stations)
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Virgin Trains Services
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											30 at Manchester Piccadilly, reduced to 27 minutes for a maximum of two turnarounds in each hour. 25 at Liverpool Lime Street. 20 at all locations for train entering passenger service after an ECS move or an ECS move following a train leaving passenger service.
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row margin-left="1mm">
									<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											* Class 220/221 - 17 minutes at Blackpool North, Liverpool Lime Street and Manchester Piccadilly.
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row margin-left="1mm">
									<fo:table-cell font-weight="bold" number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Platform End Conflicts
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											First movement
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Second movement
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Margin
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Arrive
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Depart
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											2
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Depart
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Arrive
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											3
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row margin-left="1mm">
									<fo:table-cell font-weight="bold" number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Platform Reoccupation
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Following EMU/DMU in same direction
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned='2' border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											3
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											LH/HST trains in same direction. All trains in opposite directions.
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											4
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row margin-left="1mm">
									<fo:table-cell font-weight="bold" number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Reoccupation of Single Lines
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											3 minutes. Where two trains A and B cross when A is the first train to arrive, its arrival must be a minimum of 2 1/2 minute before the arrival of train B. Train A can depart 1 minute after the arrival of Train B.
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Reversal
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											DMU/EMU
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned='2' border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											4
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 378
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned='2' border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											6
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 22x
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned='2' border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											5 CrossCountry Trains only, 6 Virgin Trains only
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											2x Class 22x
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned='2' border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											6 CrossCountry Trains only, 7 Virgin Trains only
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Class 390
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned='2' border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											15 minutes at all locations. Applies to both 9-car and 11-car units
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											HST including New Measurement Train
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned='2' border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											7
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											DVT
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned='2' border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											8 Only applies to services operating in DVT mode. 
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row margin-left="1mm">
									<fo:table-cell font-weight="bold" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											Train Crew Change Allowance
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned='2' border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											2 minutes, except: 1 minute for Merseyrail DC service and 3 minutes for Virgin Trains
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block space-before="3mm"/>
							<fo:block font-weight="bold">THE FOLLOWING PAGES SHOW THE EXCEPTIONS TO THESE STANDARD VALUES</fo:block>
					<fo:block space-before="30mm"/>

<!-- Start of Junction Margin General Table -->
					<xsl:for-each select="junctionMargins/location">
					<fo:table table-layout="fixed" width="100%" font-size="10pt" border-color="black" border-width="0.35mm" border-style="solid" text-align="left" display-align="center" space-after="5mm">
						<fo:table-column column-width="proportional-column-width(50)"/>
						<fo:table-column column-width="proportional-column-width(50)"/>
						<fo:table-column column-width="proportional-column-width(15)"/>
						<fo:table-body font-size="100%">
							<fo:table-row height="3mm" font-weight="bold" border-color="black" border-width="0.35mm" border-style="solid" margin-left="1mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block><xsl:value-of select="locationName"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<xsl:choose>
								<xsl:when test="adjSRT=''">
								</xsl:when>
								<xsl:otherwise>
									<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												<xsl:value-of select="adjSRT"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
<!-- Start of Movement Down -->
									<xsl:choose>
										<xsl:when test="movementDownHeader=''">
										</xsl:when>
									<xsl:otherwise>
									<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="movementDownHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="reasonHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="valueHeader"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="movementDown"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downReason"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downValue"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:choose>
										<xsl:when test="movementDown2=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="movementDown2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downReason2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downValue2"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="movementDown3=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="movementDown3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downReason3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downValue3"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="movementDown4=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="movementDown4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downReason4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downValue4"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="movementDown5=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="movementDown5"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downReason5"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downValue5"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="movementDown6=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="movementDown6"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downReason6"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="downValue6"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
<!-- Start of Movement Up -->
						<xsl:choose>
										<xsl:when test="movementUpHeader=''">
										</xsl:when>
									<xsl:otherwise>
								<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="movementUpHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="reasonHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="valueHeader"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="movementUp"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upReason"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upValue"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:choose>
										<xsl:when test="movementUp2=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="movementUp2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upReason2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upValue2"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="movementUp3=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="movementUp3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upReason3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upValue3"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="movementUp4=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="movementUp4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upReason4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="upValue4"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
<!-- Start of Dwell Time -->
						<xsl:choose>
										<xsl:when test="dwellTimeHeader=''">
										</xsl:when>
									<xsl:otherwise>
								<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												<xsl:value-of select="dwellTimeHeader"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="dwellTimeTrain"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="dwellTimeData"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:choose>
										<xsl:when test="dwellTimeTrain2=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="dwellTimeTrain2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="dwellTimeData2"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="dwellTimeTrain3=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="dwellTimeTrain3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="dwellTimeData3"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="dwellTimeTrain4=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="dwellTimeTrain4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="dwellTimeData4"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
<!-- Start of Junction Margins -->
						<xsl:choose>
										<xsl:when test="junctionMarginsHeader=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
										<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
										<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												<xsl:value-of select="junctionMarginsHeader"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								<fo:table-row font-weight="bold" margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="firstMovementHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="secondMovementHeader"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="marginHeader"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="firstMovement"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="secondMovement"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="margin"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:choose>
										<xsl:when test="firstMovement2=''">
										</xsl:when>
									<xsl:otherwise>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="firstMovement2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="secondMovement2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="margin2"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="firstMovement3=''">
										</xsl:when>
									<xsl:otherwise>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="firstMovement3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="secondMovement3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="margin3"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="firstMovement4=''">
										</xsl:when>
									<xsl:otherwise>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="firstMovement4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="secondMovement4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="margin4"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="firstMovement5=''">
										</xsl:when>
									<xsl:otherwise>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="firstMovement5"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="secondMovement5"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="margin5"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
<!-- Start of Turnaround Allowance -->
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceHeader=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
										<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												<xsl:value-of select="turnaroundAllowanceHeader"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceInfo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceData"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:choose>
										<xsl:when test="turnaroundAllowanceInfo2=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceInfo2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceData2"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceInfo3=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceInfo3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceData3"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceInfo4=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceInfo4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceData4"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceInfo5=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceInfo5"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceData5"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceInfo6=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceInfo6"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceData6"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceInfo7=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceInfo7"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceData7"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
<!-- Start of Turnaround Allowance (Changing direction) -->
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceChangingHeader=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
										<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												<xsl:value-of select="turnaroundAllowanceChangingHeader"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingInfo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingData"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:choose>
										<xsl:when test="turnaroundAllowanceChangingInfo2=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingInfo2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingData2"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceChangingInfo3=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingInfo3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingData3"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceChangingInfo4=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingInfo4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingData4"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceChangingInfo5=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingInfo5"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingData5"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceChangingInfo6=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingInfo6"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingData6"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceChangingInfo7=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingInfo7"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceChangingData7"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
<!-- Start of Turnaround Allowance (Same direction) -->
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceSameHeader=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
										<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												<xsl:value-of select="turnaroundAllowanceSameHeader"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameInfo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameData"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:choose>
										<xsl:when test="turnaroundAllowanceSameInfo2=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameInfo2"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameData2"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceSameInfo3=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameInfo3"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameData3"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceSameInfo4=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameInfo4"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameData4"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceSameInfo5=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameInfo5"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameData5"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceSameInfo6=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameInfo6"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameData6"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
										<xsl:when test="turnaroundAllowanceSameInfo7=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameInfo7"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="turnaroundAllowanceSameData7"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
<!-- Start of Connectional Allowance -->
						<xsl:choose>
										<xsl:when test="connectionalAllowanceHeader=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												<xsl:value-of select="connectionalAllowanceHeader"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="connectionalAllowanceInfo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="connectionalAllowanceData"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
<!-- Start of Attachment of Units -->
						<xsl:choose>
										<xsl:when test="attachmentUnitsHeader=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												<xsl:value-of select="attachmentUnitsHeader"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="attachmentUnitsInfo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="attachmentUnitsData"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
<!-- Start of Detachment of Units -->
						<xsl:choose>
										<xsl:when test="detachmentUnitsHeader=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												<xsl:value-of select="detachmentUnitsHeader"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="detachmentUnitsInfo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="detachmentUnitsData"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>
<!-- Start of Minimum Turnaround -->
						<xsl:choose>
										<xsl:when test="minimumTurnaroundHeader=''">
										</xsl:when>
									<xsl:otherwise>
										<fo:table-row height="3mm">
								<fo:table-cell number-columns-spanned="3" border-color="black" border-width="0.35mm" border-style="solid">
									<fo:block></fo:block>
								</fo:table-cell>
								</fo:table-row>
								<fo:table-row height="3mm" font-weight="bold" margin-left="1mm">
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												<xsl:value-of select="minimumTurnaroundHeader"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								<fo:table-row margin-left="1mm">
									<fo:table-cell border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="minimumTurnaroundInfo"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-color="black" border-width="0.35mm" border-style="solid">
										<fo:block>
											<xsl:value-of select="minimumTurnaroundData"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:otherwise>
						</xsl:choose>




						</fo:table-body>
					</fo:table>
<!-- End of Junction Margin General Table -->
					<fo:block/>
					</xsl:for-each>
					<fo:block id="end-of-document">
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
